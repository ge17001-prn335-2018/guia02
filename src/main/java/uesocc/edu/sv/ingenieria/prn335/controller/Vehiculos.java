package uesocc.edu.sv.ingenieria.prn335.controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Vehiculos {

    /**
     * Metodo el cual recive un archivo de texto .txt y devuelve un arrayList
     * con los elementos
     *
     * @param archivo recive un archivo .txt
     * @return devuelve un arrayList con el contenido de el archivo
     * @throws IOException puede generar una Excepcion de tipo Io por no
     * encontrar el archivo
     */
    public List<String> crearLista(InputStream archivo) throws IOException {
        List<String> lista = new ArrayList<>();
        BufferedReader texto = new BufferedReader(new InputStreamReader(archivo));
        String line = null;
        while ((line = texto.readLine()) != null) {
            lista.add(line);
        }
        return limpiarLista(lista);
    }

    /**
     * Limpia la lista eliminando las posiciones innecesarias
     *
     * @param lista recive una lista
     * @return devuelve una lista con menos posiciones
     */
    public List<String> limpiarLista(List<String> lista) {
        List<String> retorno = lista;
        for (int i = 0; i < 4; i++) {
            retorno.remove(0);
        }
        retorno.remove((retorno.size() - 1));
        retorno.remove((retorno.size() - 1));
        return retorno;
    }

    /**
     * Metodo que discrimina placas validas y devuelve placas invalidas
     *
     * @param listaPlacas recive un arrayList con el valor de las placas
     * @return devuelve un arrayList con las placas invalidas
     */
    public List<String> placasInvalidas(List<String> listaPlacas) {
        List<String> retorno = new ArrayList<>();
        Pattern coincidencia = Pattern.compile("\\s{0,1}[O,N,CD,P,A,C,V,PR,T,RE,AB,MB,M,D,E]{1,2}\\s{1,3}\\d{1,3}-\\d{3}");
        for (int i = 0; i < listaPlacas.size(); i++) {
            Matcher matcher = coincidencia.matcher((CharSequence) listaPlacas.get(i));
            boolean cadenaValida = matcher.matches();
            if (cadenaValida) {
            } else {
                retorno.add(listaPlacas.get(i));
            }
        }
        return retorno;
    }

}
